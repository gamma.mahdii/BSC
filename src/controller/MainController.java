package controller;

import exception.PaymentFormatException;
import exception.RatesFormatException;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.util.Scanner;
import model.MainModel;

/**
 *
 * @author petrh
 */
public class MainController {

    private static MainController instance;
    private final MainModel model;
    PaymentTrackerController pt;

    /**
     * SINGLETON
     *
     * @param model - the MainModel of the application
     * @return instance of MainCOntroller
     */
    public static MainController getInstance(MainModel model) {
	synchronized (MainController.class) {
	    if (instance == null) {
		instance = new MainController(model);
	    }
	}
	return instance;
    }

    /**
     * Private constructor - for singleton
     *
     * @param model - the MainModel of the application
     */
    private MainController(MainModel model) {
	this.model = model;
    }

    /**
     * @param filePath the path to the file with payment records
     */
    public void readFile(String filePath) {
	BufferedReader inReader;
	try {
	    inReader = new BufferedReader(new InputStreamReader(new FileInputStream(new File(filePath))));
	    String line;

	    while ((line = inReader.readLine()) != null) {
		checkAndSavePayment(line);
	    }

	} catch (FileNotFoundException ex) {
	    System.err.println(filePath + " file not found");
	} catch (IOException ex) {
	    ex.printStackTrace();
	}
    }

    /**
     * @param filePath the path to the file with USD rates
     */
    public void readUSDRatesFile(String filePath) {
	BufferedReader inReader;
	try {
	    inReader = new BufferedReader(new InputStreamReader(new FileInputStream(new File(filePath))));
	    String line;

	    while ((line = inReader.readLine()) != null) {
		checkAndSetUsdRate(line);
	    }

	} catch (FileNotFoundException ex) {
	    System.err.println(filePath + " file not found");
	} catch (IOException ex) {
	    ex.printStackTrace();
	}
    }

    /**
     * Method for reading console input
     */
    public void readConsole() {
	Scanner scanner = new Scanner(System.in);

	while (scanner.hasNext()) {
	    String line = scanner.nextLine();

	    if (line.equals("quit")) {
		endPaymentTracker();
		return;
	    }
	    checkAndSavePayment(line);
	}
    }

    /**
     * Start of PaymentTracker Thread
     *
     * @param interval of printing output
     */
    public void startPaymentTracker(int interval) {
	pt = new PaymentTrackerController(model, interval);
	pt.start();
    }

    /**
     * End of PaymentTracker Thread
     */
    public void endPaymentTracker() {
	if (!pt.isInterrupted()) {
	    pt.interrupt();
	}
    }

    /**
     * Check if the paymentString is well Formatted, if yes then updates the
     * value
     *
     * @param paymentString the paymentString line
     */
    private void checkAndSavePayment(String paymentString) {
	try {
	    String[] parts = paymentString.split(" ");

	    if (parts.length != 2) {
		throw new PaymentFormatException();
	    }

	    String currency = parts[0];

	    // CHECK: currency code format
	    if (currency.length() != 3 || !currency.equals(currency.toUpperCase())) {
		throw new PaymentFormatException();
	    }

	    // CHECK: Value is a number
	    BigDecimal value = new BigDecimal(parts[1]);

	    model.updateCurrencyBalance(currency, value);
	} catch (PaymentFormatException | NumberFormatException ex) {
	    System.err.println("PAYMENT FORMAT EXCEPTION");
	}
    }

    /**
     * Check if the ratesString is well Formatted, if yes then updates the value
     *
     * @param ratesString the ratesString line
     */
    private void checkAndSetUsdRate(String ratesString) {
	try {
	    String[] parts = ratesString.split(" ");

	    if (parts.length != 2) {
		throw new RatesFormatException();
	    }

	    String currency = parts[0];

	    // CHECK: currency code format
	    if (currency.length() != 3 || !currency.equals(currency.toUpperCase())) {
		throw new RatesFormatException();
	    }

	    // CHECK: Value is a number
	    BigDecimal value = new BigDecimal(parts[1]);

	    model.setCurrencyUsdExchangeRate(currency, value);
	} catch (RatesFormatException | NumberFormatException ex) {
	    System.err.println("RATES FORMAT EXCEPTION");
	}
    }

}
