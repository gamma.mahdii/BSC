package controller;

import model.MainModel;

/**
 *
 * @author petrh
 */
public class START {

    public static void main(String[] args) {
	MainModel values = new MainModel();

	MainController mc = MainController.getInstance(values);

	if (args.length == 1) {
	    mc.readFile(args[0]); // First parameter is a file with payments
	}

	if (args.length == 2) {
	    mc.readFile(args[0]); // First parameter is a file with payments
	    mc.readUSDRatesFile(args[1]); // Second parameter is a file with rates
	}

	// START PaymentTrackerThread - interval in milliseconds
	int interval = 5000;
	mc.startPaymentTracker(interval);

	mc.readConsole();
    }
}
