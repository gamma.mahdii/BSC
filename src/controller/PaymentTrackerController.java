package controller;

import model.MainModel;

/**
 *
 * @author petrh
 */
public class PaymentTrackerController extends Thread {

    private final MainModel model;
    private final int interval;

    /**
     *
     * @param model - the MainModel of the application
     */
    public PaymentTrackerController(MainModel model) {
	this.model = model;
	this.interval = 60000; // Interval not set => one minute
    }

    /**
     *
     * @param model - the MainModel of the application
     * @param interval - the interval of printing output
     */
    public PaymentTrackerController(MainModel model, int interval) {
	this.model = model;
	this.interval = interval;
    }

    @Override
    public void run() {
	try {
	    while (!Thread.currentThread().isInterrupted()) {
		synchronized (model) {
		    printValues();
		}
		Thread.sleep(5000); // 60s sleep 
	    }
	} catch (InterruptedException e) {
	    // Interuption - end of program
	}
    }

    private void printValues() {
	System.out.print(model.getActualString());
    }

}
