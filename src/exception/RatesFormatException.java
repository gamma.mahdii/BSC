package exception;

/**
 *
 * @author petrh
 */
public class RatesFormatException extends Exception {

    public RatesFormatException() {
	super();
    }

    public RatesFormatException(String message) {
	super(message);
    }
}
