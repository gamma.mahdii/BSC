package model;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map.Entry;

public class MainModel {

    private volatile HashMap<String, Currency> currency = new HashMap<>();

    public void updateCurrencyBalance(String currencyCode, BigDecimal balance) {
	if (currency.containsKey(currencyCode)) {
	    Currency actualCurrency = currency.get(currencyCode);

	    actualCurrency.updateBalance(balance);

	    currency.put(currencyCode, actualCurrency);
	} else {
	    currency.put(currencyCode, new Currency(currencyCode, balance));
	}
    }

    public void setCurrencyUsdExchangeRate(String currencyCode, BigDecimal usdRate) {
	Currency actualCurrency;

	if (currency.containsKey(currencyCode)) {
	    actualCurrency = currency.get(currencyCode);
	} else {
	    actualCurrency = new Currency(currencyCode);
	}

	actualCurrency.setUsdExchangeRate(usdRate);
	currency.put(currencyCode, actualCurrency);

    }

    public BigDecimal getCurrencyValue(String currencyCode) {
	return currency.get(currencyCode).getActualBalance();
    }

    public String getActualString() {
	StringBuilder sb = new StringBuilder();

	for (Entry<String, Currency> entry : currency.entrySet()) {
	    if (entry.getValue().isGreaterThanZero()) {
		sb.append(entry.getValue().getActualBalanceString());
	    }
	}

	return sb.toString();
    }

}
